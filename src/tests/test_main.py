def test_calc(test_app):
    response = test_app.post("/calc", json={'num1': 1, 'num2': 0})
    assert response.status_code == 200
    assert response.json() == {"result": 1}

    response = test_app.post("/calc", json={'num1': 0, 'num2': 0})
    assert response.status_code == 422
