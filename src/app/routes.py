from fastapi.responses import HTMLResponse
from fastapi import Request, WebSocket, WebSocketDisconnect, APIRouter
from fastapi.templating import Jinja2Templates

from socket_manager import manager
from kafka_manager import kafka_manager


router = APIRouter()
templates = Jinja2Templates(directory="/home/phil-note/workspace/demo/fast_api_demo/src/templates")


@router.get("/", response_class=HTMLResponse)
async def chat(request: Request):
    print(f'Sendding message with value')
    await kafka_manager.producer.send_and_wait(topic=kafka_manager.KAFKA_TOPIC, value=str.encode('ok'))
    return templates.TemplateResponse("chat.html", {'request': request, 'app_port': 8778})


@router.websocket("/ws/{client_id}")
async def websocket_endpoint(websocket: WebSocket, client_id: int):
    await manager.connect(websocket)
    try:
        while True:
            data = await websocket.receive_text()
            await manager.send_personal_message(f"You wrote: {data}", websocket)
            await manager.broadcast(f"Client #{client_id} says: {data}")
    except WebSocketDisconnect:
        manager.disconnect(websocket)
        await manager.broadcast(f"Client #{client_id} left the chat")




