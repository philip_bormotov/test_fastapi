from pydantic import BaseSettings, Field


class Settings(BaseSettings):
    host: str = Field('0.0.0.0', description='Хост сервера')
    port: int = Field(8778, description='Порт сервера')

    class Config:
        env_file = ".env"


settings = Settings()
